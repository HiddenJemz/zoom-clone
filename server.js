//To create a server based on the express server and
//passes that socket.io tell it how to interact with it
const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const { uuid } = require('uuidv4')

//For the view engine
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.get('/', (req, resp) => { resp.redirect('/${uuidv4()}') })

//To create a room
app.get('/:room', (req, resp) => { resp.render('room', {roomID: req.params.room}) })

//Have the server listen on port 3000
server.listen(3000)
console.log(uuid());

