const io = require('socket.io-client');
const socketClient = io.connect('localhost:3000');

socketClient.on('connect', () => {
        socketClient.emit('npmStop');
        setTimeout(() => {
                process.exit(0); }, 1000);
});
