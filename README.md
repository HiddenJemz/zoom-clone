# Zoom Clone

Creating a video chat (Zoom clone) tutorial for teaching new programmers how to build a video conference site. \
Date: September 2020
Retouch: June 2024

## This project version setup is for Linux based distros
To begin, start with `npm init -y` NPM is a package manager for the JavaScript language.
1. Verify that Node.js & npm are installed from command line. Versions will change with time.
``` bash
$ node -v
v14.10.0
$ npm -v
6.14.7

# Initialize NPM
$ npm init -y
```
* [Guide to install Node.js & npm](https://lbry.tv/@techmespot:c/how-to-install-node-js-and-npm-on-ubuntu:b)

``` bash
# Arch Based Distros 
$ sudo pacman -S nodejs npm

# Debian Based Distros
$ sudo apt install nodejs npm

# Initialize NPM
$ npm init -y
```
2. Install dependencies for npm
* For the server used: _express_
* For the templating language: _ejs_
* To communicate between each server: _socket.io_
* To create the dymanic URLs for the video chat rooms: _uuidv4_
* To immediately refresh the server after changes has been made: _--save-dev nodemon_
* For Peer server to accept new users to the room: _peer_

``` bash
# Install dependencies
$ npm i express ejs socket.io uuidv4

# Server refresh
$ npm i --save-dev nodemon

# Install peerjs
$ npm i -g peer
```

## Tutorial and Reference
Reference **PeerJS:** https://peerjs.com \
Reference **NodeJS:** https://nodejs.org \
Reference **NPM:** https://www.npmjs.com

Guided by the tutorial from [Web Dev Simplified](https://lbry.tv/@HiddenJemz:0/create-a-zoom-clone:0)

## To resolve open Port 3000 issue
1. Using the terminal to retrieve the PID (Process ID): `sudo lsof -i :3000`
2. Terminate the process using the PID: `kill -9 {The PID}` **Do NOT include the curely brackets**
3. Continue testing according to the tutorial guide.
4. Will continue in 2022.
